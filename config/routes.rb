Rails.application.routes.draw do
  get '/territories/:id/animals', to: 'territories#animals'
  resources :territories
  resources :animals
  # get '/animals', to: 'animals#index'
  # get '/animals/:id', to: 'animals#show'
  # post '/animals', to: 'animals#create'
  # delete '/animals/:id', to: 'animals#destroy'
  # put '/animals/:id', to: 'animals#update'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/', to: 'static_pages#home'
end

class Animal < ApplicationRecord
    validates :name, presence: true
    validate :only_adult
    belongs_to :territory
    def age
        (( Date.today - birthdate)/365).to_i
    end

    private
        def only_adult
            if age < 18
                errors.add(:lucas, " too younger")
            end
        end
end
